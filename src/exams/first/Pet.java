package exams.first;
// Pet UML programm.
import labs.lab4.Class;

public class Pet {
	private String name;
	private int age;
	
//create a default constructor
public Pet() {
	name = "";
	age = 1;		 
	}
// create a non-default constructor
public Pet (String name, int age)
{
	this.name = name;
	this.age = age;
}
// accessor methods
public String getName() {
    return name;
    }
public int getAge() {
    return age;
}
//mutator method
public void setName(String name) {
	this.name = name;
}
public void setAge(int age) {
	this.age = age;
}
@Override
public String toString(){
	return("("+this.name+","+this.age+")");	
}
@Override
public boolean equals(Object obj) {

	if (!(obj instanceof Pet)) {
		return false;
	}
	
	else  {
		Pet c = (Pet)obj;
		if (this.name ==(c.getName()) && this.age == (c.getAge())){
			return true;
		}else {
		     return false;
		}
}
}
}

