package exams.first;

import java.util.Scanner;

public class Question2_Selection {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Divisible problem 
		Scanner input = new Scanner (System.in);
		System.out.print("Please enter an integer: ");
		int num = Integer.parseInt(input.nextLine());
		if (num % 3 == 0 && num % 2 == 0) {
			System.out.println("foobar");
		}
		if (num % 2 == 0 && num % 3 != 0) {
			System.out.println("foo");
		}
		else if (num % 2 != 0 && num % 3 == 0) {
			System.out.println("bar");
		}
		else {
			System.out.println("");
		}
		input.close();
		
	}

}
