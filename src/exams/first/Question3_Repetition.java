package exams.first;

import java.util.Scanner;

public class Question3_Repetition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// triangle program
		Scanner input = new Scanner (System.in);
		System.out.println("Enter a number: ");
		int n = Integer.parseInt(input.nextLine());
		for (int i=n; i>=0;i--) { // handle the row
			for (int c=n-i; c>0; c--) { // handle the space before the star
				System.out.print(" "+ " ");
			}
			for (int j=i; j>=1; j--) {
				System.out.print(" *");
			}
			System.out.println();
		}
		input.close();
	}
}
		

	

