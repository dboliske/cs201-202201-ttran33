package exams.first;

import java.util.Scanner;

public class Queston1_DataType {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// a program that prompts the user for an integer, add 65 to it, convert the result to a character and print that character to the console
		Scanner input = new Scanner (System.in);
		System.out.println ("Please enter a number: ");
		int number = Integer.parseInt(input.nextLine());
		number = number + 65;
		char character = (char)number;
		System.out.println(character);
		input.close();
	}

}
