package exams.second;
//Write the class Circle.java
public class Circle extends Polygon {
	
private double radius;

//default constructor
public Circle () {
	this.radius =1;
}

//custom constructor
public Circle (String name, double radius) {
	super (name);
	this.radius = radius;
}

//accessor method
public void getRadius (double radius) {
	if (radius >0) {
		this.radius = radius;
	}
}

//mutator methods
public double setRadius(double radius) {
	return radius;
}

// area method call
public double area() {
	double circleArea = Math.PI * radius * radius;
	return circleArea;
}

// perimeter method call
public double perimeter() {
	double circlePerimeter = radius * 2.0 * Math.PI ;
	return circlePerimeter;
}


// toString format
@Override
public String toString(){
return "Name: " + getName() + ", radius: " + radius + ", Area" + (Math.PI * radius * radius ) + ", Perimeter: " + (radius * 2.0 * Math.PI);

}
}
