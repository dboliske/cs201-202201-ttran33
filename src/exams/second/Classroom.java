package exams.second;
//Write the class Classroom.java
public class Classroom {
protected String building;
protected String roomNumber;
private int seats;

//default constructor
public Classroom () {
	this.building = "";
	this.roomNumber = "";
	this.seats = 1;
	
}// custom constructor
public Classroom (String building, String roomNumber, int seats) {
	this.building = building;
	this.roomNumber = roomNumber;
	setSeats(seats);
}
	
// mutator methods
public void setSeats(int seats) {
	if (seats > 0) {// seats has to be positive, 0 is not negative or positive
		this.seats = seats;
	}
}

public String setRoomName(String roomName) {
	return roomName;
	
}

public String setBuilding(String setbuilding) {
	return building;
	
}
//accessor methods
public String getRoomNumber() {
	return roomNumber; 
}
public String getBuilding() {
	return building;
}

public int getSeats() {
	return seats;
}
// toString method
@Override
public String toString(){
    return "Building name: " + building + ", room number:" + roomNumber + ", seats" + getSeats();
}
}

