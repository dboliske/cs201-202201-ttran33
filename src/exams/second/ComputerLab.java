package exams.second;
//Write the class computer.java
public class ComputerLab extends Classroom{

	private boolean computers;
	
	//default constructor
	public ComputerLab () {
		this.computers = false;
	}
	// custom constructor
	public ComputerLab(String building, String roomNumber, int seats , boolean computers) {
		super(building,roomNumber, seats);
		this.computers = computers;
	}
	
	// mutator methods
	public void setComputer (boolean computers) {
		this.computers = computers;
	}
	 // accessor method
	public boolean getComputer (boolean computers) {
		return computers;
	}
	
	//hasComputer method
	public boolean hasComputer() {
		return computers;
	}
	
	//toString method
	@Override
	public String toString(){
	    return "Building name: " + building + ", room number:" + roomNumber + ", seats" + getSeats()+ ", computer is available" + computers;
	}
	
}
