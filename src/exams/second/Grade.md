# Final Exam

## Total

92.5/100

## Break Down

1. Inheritance/Polymorphism:    19.5/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  4.5/5
    
2. Abstract Classes:            18/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                5/5
    - Methods:                  3/5
   
3. ArrayLists:                  20/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  5/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        15/20
    - Compiles:                 5/5
    - Jump Search:              5/10
    - Results:                  5/5

## Comments

1.hasComputers method should return boolean
2.verification for the height and width for rectangle class is wrong and switched the body of the getter and the setter for the Circle class
3.Good
4.Good
5.Jump search should be recursive
