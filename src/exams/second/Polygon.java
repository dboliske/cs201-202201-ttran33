package exams.second;


public abstract class Polygon {
protected String name;

public Polygon () {
	this.name = "";
}
public Polygon (String name) {
	this.name = name;
}
public String setName(String name) {
	return name;
	
}
public String getName() {
	return name; 
}

public abstract double area();
public abstract double perimeter();


// to String
@Override
public String toString() {
	return "Polyogon";
}
}


