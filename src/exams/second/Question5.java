package exams.second;

import java.util.Scanner;

public class Question5 {
	//Java program that implement the Jump Search Algorithm recursively for an Array (or ArrayList) of numbers and allows the user to search for a value
public static int jumpSearch(double[] number,double search) {
	int y = number.length;
	
	// Finding block size for being jumped after that
	int jump = (int)Math.floor(Math.sqrt(y));
	
	// Finding the block where element locate at ( where is the position)
	// Present (if it is present)
     int b = 0;
     while (number[Math.min(jump, y)-1]< search)
     {
    	 b = jump;
    	 jump+= (int)Math.floor(Math.sqrt(y));
    	 if (b>=y)
    		 return -1;
     }
     
     
     //Linear Search for x in block, beginning with b
     while(number[b]<search)
     {
    	 b++;
    	 // Return -1 if element cannot be found
    	 if(b == Math.min(jump, y))
    		 return -1;
    	 System.out.println ("Cannot find number");
     }
     
     //If programm can found element
	if (number [b] == search)
		return b;
	return -1;
}

	// Testing
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean finish = false;
		try {
		do {
        double number [] = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
        Scanner input = new Scanner (System.in);
        System.out.println ("Enter number wanted to find: ");
        Double search = Double.parseDouble(input.nextLine());
        
        //Find index of "search" using jump search
        int index = jumpSearch(number, search);
        
        
        // Print out position
        System.out.println("Number " + search + " is located at index: " + index);
        System.out.println("Do you want to continue? (Enter 'y' for yes, enter 'n' for no): ");
        String answer = input.nextLine();
        if (answer.equalsIgnoreCase("y")) {
        	finish = false;
        }
        else {
        	finish = true;
        	System.out.println (" Program end!");
        }
		}while (!finish);
	}catch (Exception e) {// in case user put wrong input (e.g abc)
		System.out.println ("Wrong input");
	}
		
	}
}
