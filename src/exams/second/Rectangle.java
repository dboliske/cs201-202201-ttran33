package exams.second;
//Write the class Rectangle.java
public class Rectangle extends Polygon {

	private double width;
	private double height;
	
	//default constructor
	public Rectangle () {
		this.width = 1;
		this.height = 1;
	}
	
	//custom constructor
	public Rectangle (String name, double width, double height) {
		super (name);
		this.width = width;
		this.height = height;
	}
	
	//mutator methods
	public void setWidth (double width) {
		if (width <0) {
			this.width = width;
		}
	}
	
	//accessor method
	public double getWidth() {
		return width;
	}
	
	//mutator methods
	public void setHeight (double height) {
		if (height <0) {
			this.height = height;
		}
	}
	
	//accessor method
	public double getHeight() {
		return height;
	}
	
	//area method call
	public double area() {
		double recArea = height * width;
		return recArea;
	}
	
	// perimeter method call
	public double perimeter() {
		double recPerimeter = 2.0 * (height + width);
		return recPerimeter;
	}
	
	//toString format
	@Override
	public String toString(){
		return "Name: " + getName() + ", width: " + width + ", height: " + height + ", Area: " + (height * width) + ", Perimeter: " + (2.0 * (height + width)) ;

		}
}
