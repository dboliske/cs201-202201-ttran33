package exams.second;

public class Sorting {
	/*program that implements the Selection Sort Algorithm for an Array (or ArrayList) 
	 * of Strings and prints the sorted results.
	 */
    // Sort array
	static void selectionSort(String array [], int n) {
		// Move boundary of unsorted subarray
		for (int i = 0; i < n-1; i++) {
			// Searching for minimum element in unsorted array
			int min_index =i;
			String minString = array [i];
			for (int j = i+1; j<n; j++) {
				//CompareTo () will return a value, if String1 (array[j] is smaller than string2 (minString)
				// If array [j] is smaller than minString
				
				if (array [j].compareTo (minString)<0) {
					// Make array[j] as minString and update min_index
					minString = array [j];
					min_index = j;
				}
			}
			
			//Swapping the min element, found the first element
			if (min_index!=i) {
				String temp = array [min_index];
				array [min_index] = array [i];
				 array [i] = temp;
			}
		}
	}
	
	// Driver code	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
       String array [] = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
       int num = array.length;
       System.out.println(" The array: ");
       
       // Printing array before sorting 
       for (int i = 0; i < num; i++) {
    	   System.out.println(array[i] + ", ");
 
       }
       System.out.println();
       selectionSort(array, num);
       System.out.println("Sorted array: ");
       //Print array before sort
       for (int i =0; i<num; i++) {
    	   System.out.println(array[i]+ ", ");
       }
	}

}
