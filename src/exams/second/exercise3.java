package exams.second;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class exercise3 {
//
	public static void main(String[] args) {
		/*program that prompts the user to enter a sequence of numbers, storing them in an ArrayList,
         and continues prompting the user for numbers until they enter "Done". 
         When they have finished entering numbers, 
         your program should return the minimum and maximum values entered.
         */
		   boolean finish = false;
		   Scanner input = new Scanner(System.in);
		   ArrayList<Double> sequence = new ArrayList<Double>();// Create an ArrayList
		   
	        do {
	        	try {
	        	System.out.println("Enter a number:");// Prompt user
		        Double number = Double.parseDouble(input.nextLine());
		        sequence.add(number); // add value to the array
		        System.out.println(sequence);
	        	}catch (Exception e) {
	 			   System.out.println ("Wrong input!"); // In case user input wrong format (E.g abc instead of a number)
	 		    }
				System.out.println("Do you want to exit? Enter 'done' for exit, enter 'no' to continue: ");// Prompt user want to exit or not
				String enter = input.nextLine();
				if (enter.equalsIgnoreCase("done")) {
					finish = true;
				}
				if (enter.equalsIgnoreCase("no")) {
					finish = false;
				}
				else { // Other than input "done" will run the loop
					finish = true;
				}
				
	        }	while(!finish);
	        System.out.println("Minimum number: " + Collections.min(sequence));// give minimum number
	        System.out.println("Maximum number: " + Collections.max(sequence));// give maximum number
		   
	}
}
			
			
	        
	       




