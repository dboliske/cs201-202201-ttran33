package labs.lab1;

import java.util.Scanner;

public class Exercise1 {
// Java program that will prompt a user for a name; save the input and echo the name to the console.
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);//create scanner input
		System.out.print("Enter your name: ");// Let user to enter the name
		String value = input.nextLine();
		System.out.print("Your name is: " + value);
		
		input.close ();
		
	}

}
