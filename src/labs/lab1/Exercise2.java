package labs.lab1;

import java.util.Scanner;

public class Exercise2 {
//Output the result of the following calculations programm
	public static void main(String[] args) {
		//create scanner input
		Scanner input = new Scanner(System.in);
		// Let user to enter the age
		System.out.print("Your age is: ");
		String num1 = input.nextLine ();
		Double num = Double.parseDouble(num1);
		System.out.print("Your father's age is: ");// Let user to enter the father age
		String num2 = input.nextLine ();
		Double num3 = Double.parseDouble(num2);
		System.out.println("Distance age of you and your father is: " + (num3-num));
		System.out.print("Enter your birth year:");
		Double x = Double.parseDouble(input.nextLine());
		System.out.println (" Twice of your birth year is:" + (x*2) );
		System.out.print("Enter your height in inches: ");
		Integer y = Integer.parseInt(input.nextLine());
		System.out.println("Your height in cm is: " + (y*2.54)+ "cm");
		System.out.println ("Your height in feet is: " + ((int)y/12) + " feet " + (y%12) + " inches");
		input.close();
	}
}
