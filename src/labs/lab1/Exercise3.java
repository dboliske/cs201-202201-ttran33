package labs.lab1;
import java.util.Scanner;
public class Exercise3 {
//Prompt a user for a first name; display the user's first initial to the screen. 
	public static void main(String[] args) {
		// create scanner input	
    Scanner input = new Scanner (System.in);
    System.out.print("Enter your first name: ");// Let user to enter first name
    char c = input.nextLine().charAt(0);
    System.out.print("Your first initial is: " + c);
	input.close();
    
	}

	}


