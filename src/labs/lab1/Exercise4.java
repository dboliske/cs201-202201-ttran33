package labs.lab1;
import java.util.Scanner;
public class Exercise4 {
//Converting temperature programm
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* Farenheit to Celsius convertion:
		 * Estimate:
		 * input1: 70 degrees Farenheit, expected result 21,1111 degrees C
		 * input2: 30 degrees, expected result -1,1111 degrees C
		 * input 3 32 degrees, expected result 0 degree C
		 * Actual result ( on computer )
		 *  actual input1: 21,11111 degree(s)
		 * actual input2: -1,111111111 degree(s)
		 * actual input3: 0.0 degree(s)
		 * The result is correct
		 * Celsius to Farenheit convertion:
		 * input1: 70 degrees C, expected result 158 degrees F
		 * input2: 32 degrees C, expected result 89.6 degrees F
		 * input 3 0 degrees C, expected result 32 degree F
		 * Actual input1: 70 degrees C, expected result 158 degrees F
		 * actual input2: 32 degrees C, expected result 89.6 degrees F
		 * actual input 3 0 degrees C, expected result 32 degree F
		 * The result is correct
		 */
		//create scanner input
		Scanner input = new Scanner (System.in);
		System.out.print("Enter temperature in Farenheit: "); // Prompt user Faren temp
		Double x = Double.parseDouble(input.nextLine());
		// Converting and print the result
		System.out.println("Temperature in Celsius is: " + ((x-32)*5/9) + " degree(s)" );
		System.out.print("Enter temperature in Celsius: ");// Prompt user Celcius temp
		Double y = Double.parseDouble(input.nextLine());
		// Converting and print the result
		System.out.println("Temperature in Farenheit is: " + ((1.8*y)+32) + " degrees");
		input.close();
		
		
	}


	}


