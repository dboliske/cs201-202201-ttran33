package labs.lab1;
import java.util.Scanner;
public class Exercise5 {
//Prompt the user for the length, width, and depth in inches of a box.
	public static void main(String[] args) {
        // create scanner input, then ask them for inputing the data, then calculate the data
		/* Test plan:
		 * Input 1: l=2feet, w=3feet, h=4feet, wood needed (expect: 52 feet) ( from calculator)
		 * Input 2: l=1feet, w=1feet, h=1feet, wood needed (expect: 6 feet) ( from calculator)
		 * Input 3: l=12feet, w=15feet, h=18feet, wood needed (expect: 1332 feet) ( from calculator)
		 * Actual result:
		 * Input 1: 52 feet 
		 * Input 2: 6 feet
		 * Input 3: 1332 feet
		 * So the method is correct as I got the exact same number!
		 */
		Scanner input = new Scanner (System.in);//create scanner input
		System.out.print("Enter the length (in feet) : "); // Prompt user the length 
		Double l = Double.parseDouble(input.nextLine());
		System.out.print("Enter the width (in feet): ");// Prompt user the width
		Double w = Double.parseDouble(input.nextLine());
		System.out.print("Enter the height (in feet): ");// Prompt user the feet
		Double h = Double.parseDouble(input.nextLine());
		// Converting
		Double lengthfeet = l/12; 
		Double widththfeet = w/12;
		Double depthfeet = h/12;
		// Print result
		System.out.println("The amount of wood needed is: " + (2*lengthfeet*widththfeet+2*lengthfeet*depthfeet+2*depthfeet*widththfeet) + " square feet");
		input.close();
	
		
	}

}
