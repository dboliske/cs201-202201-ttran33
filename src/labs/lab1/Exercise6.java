package labs.lab1;
import java.util.Scanner;
public class Exercise6 {
//a program that will convert inches to centimeters
	public static void main(String[] args) {
		
		
		/*
		 * Estimates result:
		 * Input 1: 3 inches, estimated result: 7.62 cm
		 * Input 2: 15 inches, estimated result: 38.1 cm
		 * Input 3: 725 inches, estimated result: 1841.5 cm
		 * Result ( from the program )
		 * Input 1: 3 inches, result: 7.62 cm
		 * Input 2: 15 inches, result: 38.1 cm
		 * Input 3: 725 inches, result: 1841.5 cm
		 * The result is correct, so the program goes well!
		 */
		//create scanner input
		Scanner input = new Scanner (System.in);
		System.out.print("How many inches you want to convert? ");// user enter the data
		Double x = Double.parseDouble(input.nextLine()); // save inout user
		// print out and convert result
		System.out.println( x + " inches in cm is: " + (x*2.54)+ " cm");
		input.close();
	}

}
