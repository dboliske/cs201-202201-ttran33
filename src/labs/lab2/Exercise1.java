package labs.lab2;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Write a Java program that will prompt the user for a number and print out a square with those dimensions. 
		Scanner input = new Scanner(System.in); // get user input
			
			System.out.print("Enter size you want:  ");
			int size = Integer.parseInt(input.nextLine());
			
			for (int row=0; row<size; row++) {
				for (int col=0; col<size; col++) {
					System.out.print("* ");// display square with character of *
				}
				System.out.println(); 
			}
			
		}

	
	}


