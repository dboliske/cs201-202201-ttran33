package labs.lab2;
import java.util.Scanner;
public class Exercise2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//Write a Java program that will prompt the user for the grades for an exam, computes the average, and returns the result
		Scanner input = new Scanner(System.in); // get user input
		int total = 0,n; 
		int count = 0;
	
		 while  (true) {// infinite loop
	            System.out.print("Enter grade (-1 to finish entering): ");
	            n = input.nextInt();
	            if (n == -1) {
	                break;// if user enter -1, the program will be out
	            }
	            total = total + n;
	            count++;
	        }
	        float average = total / count;
	        System.out.println("Average: " + average);
	    }
	}
/* Try some inputs:
 * input1 : 90
 * input2 : 100
 * input 3:  70
 * input 4: -1
 * Average: 86.0
 * input 1: 90
 * input 2: 90
 * input 3: 100
 * input 4: -1
 * Average: 93.0
 * input 1: 90
 * input 2: 95
 * input 3: 89
 * input 4: -1
 * average: 91.0
 */
 
