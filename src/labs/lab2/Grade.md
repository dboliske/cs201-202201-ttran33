# Lab 2

## Total

19.5/20

## Break Down

* Exercise 1    6/6
* Exercise 2    5.5/6
* Exercise 3    6/6
* Documentation 2/2

## Comments
For exercise 2, the average is not quite accurate because the total shoul have been assigned to a double or float not an int.
Good work overall.