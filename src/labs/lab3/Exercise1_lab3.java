package labs.lab3;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
public class Exercise1_lab3 {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		//Write a program that reads in the file and computes the average grade for the class and then prints it to the console
		
		File f = new File("src/labs/lab3/grades.csv");
		Scanner input = new Scanner (f);
		int[] grade = new int[1];
		int count = 0;
		double total = 0; // has to declare as a double as well so that it fit with the double value
		int i;
		while (input.hasNextLine()) {
			String [] value = input.nextLine().split(",");// take everything but that character
			//Double val = Double.parseDouble(value);
			
		   total = total + Double.parseDouble(value[1]);
			//Integer value = Integer.parseInt(value)
			count++;
		}
		System.out.println(" Average: " +(total/count));
input.close();
		
	}

}
