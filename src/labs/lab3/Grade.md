# Lab 3

## Total

18/20

## Break Down

* Exercise 1    6/6
* Exercise 2    4/6
* Exercise 3    6/6
* Documentation 2/2

## Comments
Ex2: You needed to create an array of very small size in the beginning and then create another array within your code that increments its size as long as there is more input. This array serves as a tool and is assigned to the old array when there is no more input. 
Good work overall.