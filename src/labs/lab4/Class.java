package labs.lab4;
//Write the class Potion.java
public class Class {
	 String name;
	 double strength;
	 
	 //create a default constructor
	public Class()
	{   
	this.name = "";
	this.strength = 0;
	}
	
	//create a non-default constructor
    public Class (String name, double strength)
    {
    	this.name = name;
    	this.strength = strength;
    }
    
    // accessor methods
    public String getName() {
    return name;
    }
    
    public double getStrength() {
        return strength;
    }
    
    // mutators method
    public void setName(String name) {
    	this.name = name;
    }
    public void setStrength(double strength) {
    	this.strength = strength;
    }
    
    @Override
    public String toString(){
    	return("("+this.name+","+this.strength+")");	
    }
 // return true, if latitude is in valid range (-90 to 90)
    public boolean isValidLat(){
        if(this.strength>=0 && this.strength<=10){
            return true;
        }
        else{
            return false;
        }
        }
        @Override
    	public boolean equals(Object obj) {

    		if (!(obj instanceof Class)) {
    			return false;
    		}
    		
    		else  {
    			Class c = (Class)obj;
    			if (this.name ==(c.getName()) && this.strength == c.getStrength())
    				return true;
    			else
    			     return false;
    			}
    }
    
}
