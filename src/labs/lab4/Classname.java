package labs.lab4;

public class Classname {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//application class that instantiates two instances of Potion. One instance should use the default constructor and the other should use the non-default constructor
		Class Classname1 = new Class ();
		Classname1.setName("Thu");// setting values using matator method 
		Classname1.setStrength(9.0);
		
		//Assinging values for lat and lng to create new Geolocation 2
		double strength = 8.5;
		String name = "Thu";
		
		// Creating object using values using non-default 
		Class Classname2 = new Class (name,strength);
		System.out.println ("Class 1: ");
		System.out.println (Classname1.toString());
		System.out.println ("\n Class 2: ");
		System.out.println (Classname2.toString());
	}
}
