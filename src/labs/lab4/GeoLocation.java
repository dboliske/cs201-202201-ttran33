package labs.lab4;
//Write the class GeoLocation.java
public class Geolocation {
	    double lat;
	    double lng;
	    
	    // default constructor
	    public Geolocation(){
	        // default values will be 0
	        this.lat = 0;
	        this.lng = 0;
	    }
	    
	    // custom constructor
	    public Geolocation(double lat,double lng){
	        this.lat = lat;
	        this.lng = lng;
	    }
	    
	    // accessor methods
	    public double getLat(){
	        return lat;
	    }
	    public double getLng(){
	        return lng;
	    }
	    
	    // mutator methods
	    public void setLat(double lat){
	        this.lat = lat;
	    }
	    public void setLng(double lng){
	        this.lng = lng;
	    }
	    
	    @Override
	    public String toString(){
	        return("("+this.lat+","+this.lng+")");
	    }
	    
	    
	    // return true, if latitude is in valid range (-90 to 90)
	    public boolean isValidLat(){
	        if(this.lat<=90 && this.lat>=-90){
	            return true;
	        }
	        else{
	            return false;
	        }
	    }
	    
	    // return true, if longitude is in valid range (-180 to 180)
	    public boolean isValidLng(){
	        if(this.lng<=180 && this.lng>=-180){
	            return true;
	        }
	        else{
	            return false;
	        }
	    }
	   

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Geolocation)) {
			return false;
		}
		
		else  {
			Geolocation g = (Geolocation)obj;
			if (this.lat ==(g.getLat()) && this.lng == g.getLng())
				return true;
			else
			     return false;
			}
	}
	
	}



		
		