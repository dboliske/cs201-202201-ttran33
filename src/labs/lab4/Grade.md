# Lab 4

## Total

25.5/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7/8
  * Application Class   0.5/1
* Part II PhoneNumber
  * Exercise 1-9        8/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        6/8
  * Application Class   1/1
* Documentation         2/3

## Comments
*Part1:
  -1 Instance variables were not private 
  -0.5 you did not call the accessor methods in the application class

*Part2:
  -1 documentation missing

*Part3:
  -1 Class name was supposed to be Potion
  -1 Instance variables were not private