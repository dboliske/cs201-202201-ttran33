package labs.lab4;

public class Location {

	public static void main(String[] args) {
		// write an application class that instantiates two instances of PhoneNumber
		// Creating object using default constructor 
		Geolocation location1 = new Geolocation ();
		location1.setLat(100.5);// setting values using matator method 
		location1.setLng(90.5);
		
		//Assinging values for lat and lng to create new Geolocation 2
		double lat = 100.5;
		double lng = 90.5;
		
		// Creating object using values using non-default 
		Geolocation location2 = new Geolocation (lat, lng);
		System.out.println ("Location 1: ");
		System.out.println (location1.toString());
		System.out.println ("\n Location 2: ");
		System.out.println (location2.toString());

	}

}
