package labs.lab4;

public class PhoneNum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//// write an application class that instantiates two instances of Location
		PhoneNumber phoneNumber1 = new PhoneNumber ();
		phoneNumber1.setCountryCode("1");// setting values using matator method 
		phoneNumber1.setAreaCode("38");
		phoneNumber1.setNumber("5180138");
		
		//Assinging values for lat and lng to create new Geolocation 2
		String countryCode = "1";
		String areaCode = "0903";
		String number = "681643";
		
		// Creating object using values using non-default 
		PhoneNumber phoneNumber2 = new PhoneNumber (countryCode,areaCode,number );
		System.out.println ("PhoneNumber 1: ");
		System.out.println (phoneNumber1.toString());
		System.out.println ("\n PhoneNumber  2: ");
		System.out.println (phoneNumber2.toString());

	}

}
