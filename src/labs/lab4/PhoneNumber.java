package labs.lab4;
//Write the class PhoneNumber.java
public class PhoneNumber {
	private String countryCode, areaCode, number;
	public PhoneNumber()
	{   
	this.countryCode = "";
	this.areaCode = "";
	this.number = "";
	}
	public PhoneNumber(String countryCode, String areaCode, String number) {

		   this.countryCode = countryCode;
		   this.areaCode = areaCode;
		   this.number = number;
		}
	public String getCountryCode() {
		   return countryCode;
		}
		public String getAreaCode() {
		   return areaCode;
		}
		public String getNumber() {
		   return number;
		}
		public void setCountryCode(String countryCode) {
			   this.countryCode = countryCode;
			}
			public void setAreaCode(String areaCode) {
			   this.areaCode = areaCode;
			}
			public void setNumber(String number) {
			   this.number = number;
			}
			//part 6
			@Override
			public String toString() {
			   return "PhoneNumber: " + countryCode+"-" + areaCode + "-" + number;
			}
			public boolean isAreacode_len3()
			{if(areaCode.length()==3)
			   return true;
			return false;
			}
			public boolean isnum_len7()
			{if(number.length()==7)
			   return true;
			return false;
			}
			@Override
			public boolean equals (Object obj) {
				if (!(obj instanceof PhoneNumber)) {
					return false;
				}
				PhoneNumber p = (PhoneNumber)obj;
				if (this.countryCode ==(p.getCountryCode()) && this.areaCode == p.getAreaCode()&&this.number == p.getNumber())
					return true;
				else
				     return false;
				}
				
			}
			


				

		

