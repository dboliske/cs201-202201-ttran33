package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp  {
	// CTA Stop app
	public static CTAStation[] readFile(String filename) {
		CTAStation [] Stations = new CTAStation [34];
		int count = 0;
		try {
			File file = new File (filename);
			Scanner input = new Scanner (file);
			//Read file, take in data and convert into a double 
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String [] values = line.split(",");
				CTAStation a = null;
				if (!values[1].equals("Latitude")) {
					a = new CTAStation (values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2]),values [3], Boolean.parseBoolean(values[4]), Boolean.parseBoolean(values[5]));
					CTAStation[] station;
					count ++;
				}
			}
		}catch (FileNotFoundException fnf) {
			
		}
		return Stations;
	}
	public static CTAStation[] menu (Scanner input, CTAStation[] data) {
		boolean done = false;
		//Menu for user to choose
		do {
			System.out.println("1. Display station name ");
			System.out.println("2. Display station with/without wheelchair access ");
			System.out.println("3. Display nearest station ");
			System.out.println("4. Exit ");
			System.out.println("Choice: ");
			String choice = input.nextLine();
			switch (choice){
			case "1" : // Display station names
				displayStationNames(data);
				break;
			case "2" : // Display station with/without wheelchair
				displayByWheelchair(data);
				break;
			case "3" : // Display nearest station 
				displayNearest(data);
				break;
			case "4":
				done = true;
				break;
				default:
					System.out.println ("The input is not correct!");
			}
		}while (!done);	
		return data;
		
	}//Option 1
	private static void displayStationNames(CTAStation[] data) {
		// TODO Auto-generated method stub
		for (int i = 0; i<data.length; i++) {
			System.out.println(data[i]);
		}
	}
	
	//Option 2
	private static void displayByWheelchair(CTAStation[] data) {
		// TODO Auto-generated method stub
		int n = 0;
		Scanner input1 = new Scanner (System.in);
		System.out.println ("Wheelchair accessibility? ('y' for 'yes' or 'n' for 'no': ");
		String a = input1.nextLine ();
		//Keeping asking till user enter correct input
		while (n!=0) {
			System.out.println("Wheelchair accessibility? ('y' for 'yes' or 'n' for 'no':");
			 a = input1.nextLine ();
		}
		if (a.equalsIgnoreCase("y")) { //If user enter 'y'
			for (int i =0; i <data.length; i++) {
				if (data [i].hasWheelchair()==true) {
					System.out.println(data[i]);
				}
			}
			n = 0;}
		else {n = 1;
		}
			
	}

	//Option 3:
	private static void displayNearest(CTAStation[] data) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.print("Enter longtitude: ");
		double long1 = Double.parseDouble(input.nextLine());
		System.out.print("Enter lattitude: ");
		double lat1 = Double.parseDouble(input.nextLine());
		double min = data[0].calcDistance(lat1,long1);
		CTAStation nearestStation = data[0];
		for (int i = 0; i<data.length; i++) {
			double nearest = data [i].calcDistance(lat1,long1);
			if (nearest < min) {
				min = nearest;
				nearestStation = data [i];
			}
		}
		System.out.print(nearestStation.toString());
	}
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.print("Load file: ");
		String filename =input.nextLine();
		//Load file (if exists)
		CTAStation[] data = readFile (filename);
		//Menu
		data = menu (input,data);
		//Save file 
		input.close();
		System.out.print("Ended! ");
	}

}
