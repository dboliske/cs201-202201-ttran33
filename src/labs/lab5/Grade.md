# Lab 5

## Total

15/20

## Break Down

CTAStation

- Variables:                    2/2
- Constructors:                 1/1
- Accessors:                    2/2
- Mutators:                     2/2
- toString:                     1/1
- equals:                       0/2

CTAStopApp

- Reads in data:                1/2
- Loops to display menu:        2/2
- Displays station names:       0.5/1
- Displays stations by access:  1/2
- Displays nearest station:     1.5/2
- Exits                         1/1

## Comments
CTA station:
equals method missing

CTAStopApp:
-Reads in data: you had the right idea for the readfile, but the program still does not read the right data because of some issues in the method
-Displays station names: Since the data is being read as null, the station names could not be displayed
-Displays stations by access: You also had the right idea, but your method is missing some parts, and the data is read as null
-Displays nearest station: you aare using a method that has never been defined(calcDistance)
