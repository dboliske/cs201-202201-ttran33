package labs.lab6;
import java.util.ArrayList;
import java.util.Scanner;
public class lab6 {
	 Scanner input = new Scanner(System.in);
	 int addCustomer(ArrayList<String> customerQueue){
		System.out.print("Enter name of the customer: ");
		String name = input.nextLine();
		System.out.println("Your name is: " + name);
		customerQueue.add(name);
		return customerQueue.size();
	}
	 public void helpCustomer(ArrayList<String> customerQueue){
		 String customerName = customerQueue.remove(0);
		 System.out.println(customerName);
	 }
	 public void menu(ArrayList<String> customerQueue){
		 int choice = 0;
		 while(choice != 3){
			 System.out.println("1. Add customer to queue");
             System.out.println("2. Help customer");
             System.out.println("3. Exit");
             System.out.print("Enter your choice: "); 
             choice = Integer.parseInt(input.nextLine());
             if(choice == 1){
                 addCustomer(customerQueue); 
             }
             else if(choice == 2){
                 helpCustomer(customerQueue);
             }
             else if(choice != 3){
                 System.out.println("Please enter a valid input");
             }
		 }
	 }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 ArrayList<String> customerQueue = new ArrayList<String>();
		 lab6 deli = new lab6();
		 deli.menu(customerQueue);
	 		}
}
