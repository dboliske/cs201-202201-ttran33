package labs.lab7;
import java.util.Scanner;
public class BinarySearch {
	static int binarySearch(String[] arr, String x)
	{
	int l = 0, r = arr.length - 1;
	while (l <= r) {
	int m = l + (r - l) / 2;
	  
	int res = x.compareTo(arr[m]);
	  
	if (res == 0)   // Check if x is present at mid
	return m;
	  
	if (res > 0) // If x greater, ignore left half
	l = m + 1;
	  
	else    // If x is smaller, ignore right half   
	r = m - 1;
	}
	return -1;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] arr = { "c", "html", "java", "python", "ruby", "scala"};
		Scanner sc= new Scanner(System.in); //System.in is a standard input stream.
		System.out.print("Enter a string: ");
		String str= sc.nextLine(); //reads string
		int result = binarySearch(arr, str);
		  
		if (result == -1)
		System.out.println("Element not present");
		else
		System.out.println("Element found at "
		+ "index " + result);
	}

}
