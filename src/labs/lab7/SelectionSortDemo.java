package labs.lab7;

public class SelectionSortDemo {
	 public static void selectionSort(double[] arr)
	    {  
	        //array for sort elements
	        for (int i = 0; i < arr.length - 1; i++)  
	        {  
	            int index = i;  
	            for (int j = i + 1; j < arr.length; j++)
	            {  
	                if (arr[j] < arr[index])
	                { 
	                //searching for lowest index     
	                    index = j; 
	                }  
	            }  
	            double smallerNumber = arr[index];   
	            arr[index] = arr[i];  
	            arr[i] = smallerNumber;  
	        }  
	    }  
	    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[] arr1 = {3.142,2.718,1.414,1.732,1.202,1.618,0.577,1.304,2.685,1.282};  
        //print given array
        System.out.println("Given array :");
        System.out.print("[");  
        for(double i:arr1)
        {  
            System.out.print(i+" ");  
        }  
        System.out.println("]");  
        
        //sorting array using selection sort function call  
        selectionSort(arr1);  
        //print sorted array 
        System.out.println("\nSelection Sort :");
        System.out.print("[");
        for(double i:arr1)
        {  
            System.out.print(i+" ");  
        }
        System.out.println("]"); 
    	}  
	}

