//Create AgedRestrictedItem so that we can call it again in the method the app afterthat 
// Author's name: Thu Tran, date: May 28, 2022
package project;

public class AgeResItem  extends ShelvedItem{
	
	public int age ; // age cannot more than 1000 years old
	
	    // default constructor
		public AgeResItem () {
			this.age = 1;
		}
		// custom constructor
		public AgeResItem(String name, double price, int age) {
			super(name,price);
			this.age = age;
		}
		// mutator methods
		public void setAge (int age) {
			this.age = age;
		}
		// accessor methods
		public int getAge() {
			return age;
		}
		
		//toString format
		@Override
		public String toString() {
			return ("Product: " +  getName() + " Price: " + getPrice()  + " Restricted age: " + this.age);
		}
		
		
		 public boolean isValidAge(){
		        if(this.age >= 0){
		            return true;
		        }
		        else{
		            return false;
			}
		  }
		 public boolean equals(Object obj) {

				if (!(obj instanceof AgeResItem  )) {
					return false;
				}
				else  {
					AgeResItem  a = (AgeResItem ) obj;
					if (this.age ==(a.getAge())){
						return true;
					}
					else {
					     return false;
					}
		    }

		 }

				}
		  

		 
	
	

