package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.crypto.Data;


//Create the app for applied shelvedItem class, ProducedItem class, and AgeResItem class
public class App1 {

	
	
	

	// Create method for reading the File. 
	public static ShelvedItem[] readFile(String filename) {
		ShelvedItem[] item = new ShelvedItem[57]; // Create an array for the stock.csv
		
		int count = 0; // create an integer variables that has value is 0
		Scanner input1 = new Scanner(System.in); //Create input Scanner
		int choose =0; // create an integer variables that has value is 0
		try {
			
			File f = new File(filename); // Create name file named f for input the file after that (src/project/stock.csv).
			Scanner input2 = new Scanner(f); // create scanner input to input in the file 
			
		// Read file, take in data and convert to double:
	    // Then distinguish between ShelvedItem data,ProducedItem class, and Agestricted class by how many data they have if seperate by comma
			while (input2.hasNextLine()) { 
				try {
					String line = input2.nextLine(); // line = input when the user input data in
					String[] values = line.split(","); // split value in the file. Take everything except for the comma ("banana" , "0.62", "04/29/2022")
					ShelvedItem a = null; // create variable a take main class data ShelvedItem. If choose = 1 a will be item of ShelvedItem, choose = 2 a will be item of ProducedItem, choose = 3 would be of the other
					
					if (values.length == 2) {
						choose = 1; 
					}
					else {
						if (values[2].length() == 10) {/*data in position 3, after the comma. 
						*If data length > 10 character, it will be ProducedItem
						*/
						 choose = 2;
						}
						/*data in position 3, after the comma. 
						*If data length > 10 character, it will be  AgeRestrictedItem
						*/
						 else if (values[2].length() == 2) {
						  choose = 3;	
						}
					switch (choose) {
					case 1:// In this case, a would be data of ShelvedItem
						a = new ShelvedItem(
								values[0],
								Double.parseDouble(values[1])
						
						);
						break;
					case 2:// In this case, a would be data of ProducedItem
						a = new ProducedItem(
								values[0],
								Double.parseDouble(values[1]),
								values[2]);
					
						
						break;
					case 3:// In this case, a would be data of AgeResItem
						a = new AgeResItem(
								values[0],
								Double.parseDouble(values[1]),
								Integer.parseInt(values[2])
								
						);
						break;
						
				}		
					// resize the array
				if (item.length == count) {
					item = resize(item, item.length*2);	// resize the array again by multiply the array by 2		
				}
				
				item[count] = a; 
				count++;
			}
					// do try catch so that if there is error, the programm would not be shut down
				}catch (Exception e) {
					// Do nothing.
			}
		}
			// do try catch so that if there is error, the programm would not be shut down if they cannot find File needed to find
	} catch(FileNotFoundException fnf) {
		System.out.println ("File not found.");
	}
		item = resize (item, count); // resize the array
	
	return item;	
	}
	
	
	
	
	    //create resize method for resizing the array after the programm run user's requirement
		private static ShelvedItem[] resize(ShelvedItem[] data, int size) {
			ShelvedItem[] array = new ShelvedItem[size]; // create an array for resizing the array 
			int limit = data.length > size  ? size : data.length;
			//Create a loop
			for (int i=0; i<limit; i++) {
				array[i] = data[i];
			}
			
			return array;
	}
		
		
		
		
		// Create menu to ask user what they want the program to do? ( create/search/ update/ remove then add item to cart)
		public static ShelvedItem[] menu(Scanner input3, ShelvedItem[] data,  ArrayList<String> cart) {
			boolean done = false; // Create a boolean for the do-while method afterthat
			// Menu for user to choose
			do {
				System.out.println("1. Create item");
				System.out.println("2. Srearch for item");
				System.out.println("3. Update items: ");
				System.out.println("4. Remove item: ");
				System.out.println("5. Exit");
				String choice = input3.nextLine(); // Save what user just command the programm to run. Each choice would leads to use different method after that
				// User will enter their choice
				// Create switch choice for different methods
				switch (choice) {
				case "1": // Add item
					createItem(data,input3); // Case 1 leads to CreateItem method
					break;
				case "2": // Search item
					searchItem(data,input3);// Case 2 leads to searchItem method
					break;
				case "3": // modify item
					modifyItem(data,input3);// Case 3 leads to modifyItem method
					break;
				case "4": // remove item
					removeItem(data,input3, cart);// Case 4 leads to removeItem method
				case "5": // Exit
					done = true; // done = true so the program would end
					break;
					// In case user input the wrong format
				default:
					System.out.println("Sorry the input is not valid.");
				}
			} while	(!done); // If it is not done, then programm would run
			
			return data;
		}
		
	
		
		
		
		// Option removeItem (if user want to sell that Item)
		// Create removeItem method so when the customer enter 4, choose = 4, leading to this method
		private static int removeItem(ShelvedItem[] data, Scanner input3, ArrayList<String> cart) {
			boolean done = false;// Create a boolean for the do-while method after that
								 // Menu for user to choose
			do {
				try {
					System.out.println("Enter name of item you want to remove: "); // Enter item user want to remove
					String enter = input3.nextLine(); // Save what user just put in
					int index = -1; // Create an index and initialize it = -1 so that if the item is not found, index would be =0 so that no item is print out
		             // create a loop to find position of data customer want to remove
					for (int i2 = 0 ; i2 < data.length; i2++)
					{ 
						if(enter.equalsIgnoreCase(data[i2].getName()))	
						{
							index = i2; // Create an index and make it equal to position of i2
							cart.add(data[i2].toString()); // using arrayList created in main, and use method add to add item customer want to remove
							System.out.println("Item(s) you remove:" + data[i2].toString() );
							break;
						//String sellItem = data.remove(i2)
								}
							}
					// Removing item from the stock
					int j = 0; // initial lize variable = 0 where is the position of new array (name: copy) so that shelved item array can move data to new array except for the removed Item one
					ShelvedItem[] copy = new ShelvedItem[data.length-1];
					for (int i3 = 0 ; i3 < data.length; i3++) {
						if (index != i3) {
							copy [j] = data[i3];
							ShelvedItem item = copy [j]; // copying the array of ShelvedItem
							j++;
						}
					}
					if (index == -1) { // If index =-1 meaning the item is sold out or there are no item in the stock
						System.out.println("Item not found");
					}
					System.out.println("product in cart" + cart); // Print out the removed item (also the addtocart Item)
				}
				catch (Exception e) { // Using try catch to make sure the programm would not be error if user input wrong format. Example 123
					System.out.println ("Wrong input. Please enter a gain.");
				}
				System.out.println ("Do you want to continue removing? Enter 'y' for yes, 'n' for no:" );// Prompt user want to continue removing item?
				String answer = input3.nextLine();
				if (answer.equalsIgnoreCase ("y")) { // enter yes the loop will run again
					done = true;
				}
				else { // If not programm of this method will be exited
					done = false;
				}
					
	}while (done);
		return -1;
	}
			
		
		
		// Option Add more Item (if user want to add new item to the stock)
		// Create createItem method so when the customer enter 1, choose = 1, leading to this method
		public static ShelvedItem[] createItem(ShelvedItem[] data, Scanner input3) {
			boolean work = false; // create boolen for work to use do-while loop
			do {
			ShelvedItem a; // call variable ShelvedItem a before so that customer can add new variable to stock then categorize them
			try {// using try catch so if the customer enter wrong format, the program would not be shut down
			System.out.print("Name of item: ");// Enter name of new item
			String name = input3.nextLine(); // Save new name
			System.out.print("Item price: ");// Enter price of new item
			double price = 0; // save price
			try { // using try catch because if user enter name = 123 or price = apple the program not shut down
				price = Double.parseDouble(input3.nextLine());	
			} catch (Exception e) {
				System.out.println("That is not valid. Returning to menu.");
				return data;
			} 
			
			
			System.out.print("Type of Item (ShelvedItem, ProducedItem, or AgeResItem): "); // categorizing the new item
			String type = input3.nextLine(); // save the categories
			
				switch (type.toLowerCase()) {//if use type in as Capital format, program will change all of them to lower case for switch
				case "shelveditem":// With user choosing shelveditem, data type in will be saved under shelveditem categories
					a = new ShelvedItem(name, price);// item have form name, price (pumpkin, 3.00)
					System.out.println("Item created: "); // print out what new item in stock
					System.out.println(a);
					break;
					
					
					
				case "produceditem":// With user choosing produceditem, data type in will be saved under produceditem categories
					System.out.print("Enter expiration date in format mm-dd-yyyy: ");// enter the date
					String edate = input3.nextLine();// save date customer add
					a = new ProducedItem(name, price, edate);// item have form name, price, date (pumpkin, 3.00, 04-29-2022)
					System.out.println("Item created: ");
					System.out.println(a);// print out what new item in stock
					break;
					
					
				
				case "ageresitem":// With user choosing ageresitem, data type in will be saved under ageresitem categories
					System.out.print("Enter restricted age: ");// enter the age
					int age = Integer.parseInt(input3.nextLine());// save age customer add
					a = new AgeResItem(name, price, age);// item have form name, price, age (pumpkin, 3.00, 21)
					System.out.println("Item created: " );
					System.out.println(a);// print out what new item in stock
					break;
					
		
				default:
					System.out.println("This type of item is not available. Returning to menu.");// if not any of the categories, program will return to menu
					return data;
			}
			data = resize(data, data.length+1);//resize the array after adding new item
			data[data.length - 1] = a;
			
			
			System.out.println("Do you want to continue? Enter 'y' for yes, 'n' for no:");// ask the user if they want to continue this program
			String answer = input3.nextLine();
			if (answer.equalsIgnoreCase("y")){ // if yes the loop will run again
				work = false;// finish is not finish
			}
			else 
			{
				work = true;// finish is now finish
				System.out.println("Done!");
			}
			} catch (Exception e) {
				System.out.println ("Wrong input. Please enter a gain.");
			}
			} while (!work); // if work is not true, continue to conduct the loop. If not work, run the loop, work is work , done
			
			return data;
		}
		
		
		
		
		// Option SearchItem, (if user want to search new item to the stock)
		// Create SearchItem method so when the customer enter 2, choose = 2, leading to this method
		public static void searchItem(ShelvedItem[] data, Scanner input3) {
	try {
			System.out.println("Enter name of item you want to search: "); // prompt user what item want to search
			String enter = input3.nextLine(); // save input
			int index = -1; // initialize index = 1  so that if the item is not found, index would be =0 so that no item is print out
			// create a loop to find position of data customer want to remove
			for (int i2 = 0 ; i2 < data.length; i2++)
			{ 
				if(enter.equalsIgnoreCase(data[i2].getName()))	// Create an index and make it equal to position of i2
				{
					index = i2;
					System.out.println(data[i2].toString()); //once we found position of searching item, we know what item is
					break;
				}
			}
		}
		catch(Exception e) {
			System.out.println("Item is not found"); // if user put wrong input (e.g: 123)
		      }
		}
		
		// Option modifyItem, (if user want to modify  item to the stock)
	    // Create modifyItem method so when the customer enter 3, choose = 3, leading to this method
		public static void modifyItem(ShelvedItem[] data, Scanner input3) {
			boolean finish = false; 
			do {
			System.out.println("Enter item that you want to make adjustment: "); // Prompt user for item want to adjust
			String change1 = input3.nextLine(); // Save input
			int index = -1;// initialize index = 1 so that if the item is not found, index would be =0 so that no item is print out
			// create the loop for going through each item
				for(int i1=0; i1 <= data.length; i1++)
				{
					if(change1.equalsIgnoreCase(data[i1].getName()))
					{
						System.out.println(data[i1].toString());
						
					index = i1;	// position of index equal to i1
					System.out.println("Do you want to change price (enter p) or name (enter n) or age (enter a) or date (enter d) : ");// What user want to change
					String change2 = input3.nextLine();// save choice
					
						   
						      switch(change2) {
						
						case "p":// With case p
							System.out.println("Enter new price: "); // Prompt new price
							double newprice = Double.parseDouble(input3.nextLine());// save input
							data[index].setPrice(newprice); // go to position of index then set new price
							System.out.println("New name: " + data[index].getName()+ " New price: " + data[index].getPrice());// Print out new update 
							break;
						
						case "n":// With case n
							System.out.println("Enter new item name: ");// Prompt new name
							String newname = input3.nextLine();// save input
							data[index].setName(newname);//go to position of index then set new name
							System.out.println("New name: " + data[index].getName()+ " New price: " + data[index].getPrice());// Print out new update
							break;
							
							/*
						case "d":// With case p
							System.out.println("Enter new date (please type format mm-dd-yyyy): "); // Prompt new date
							String newdate =  input3.nextLine();// save input
							data2.get(index).setDate(newdate); // go to position of index then set new date
							System.out.println("New name: " + data[index].getName()+ " New price: " + data[index].getPrice() + "New date: " +  data2.get(index).getDate());// Print out new update 
							break;
							
							
							
						case "a":// With case p
							System.out.println("Enter new age: "); // Prompt new age
							String newage =  input3.nextLine();// save input
							data1.get(index).setAge(Integer.parseInt(newage)); // go to position of index then set new age
							System.out.println("New name: " + data[index].getName()+ " New price: " + data[index].getPrice() + "New age: " +  data1.get(index).getAge());// Print out new update 
							break;
	                     
							
							*/
						default:
							System.out.println("Item nor found"); // in case user input wrong form 
							break;
						      }
						      System.out.println("Do you want to continue? Enter 'y' for yes, 'n' for no:");// Prompt user if they want to continue
								String answer = input3.nextLine();
								if (answer.equalsIgnoreCase("y")){// if yes finish = false not finish, run loop again
									finish = false;// finish is not finish
								}
								else 
								{
									finish = true; // finish is now finish
									System.out.println("Done!");// print out done
								}
					    }
				}

			}while (!finish);// if work is not true, continue to conduct the loop. If not work, run the loop, work is work , done
							
		}
		
		
		
		
		
	

		// For reading the file (user type in the file path)
		public static void main(String[] args) {
			ArrayList<String> cart = new ArrayList<String>(); // create arrayList for cart so that customer can add item to cart when they remove item
			
			Scanner input = new Scanner(System.in); // Create Scanner input
			System.out.print("Load file (please type: src/project/stock.csv): ");
			String filename = input.nextLine(); // Save in what file user just put in
		// Load file (if exists)
		ShelvedItem[] data = readFile(filename);// data in array ShelvedItem = data of input file
		// Menu
		
		data = menu(input,data, cart); // data include input from the data, data from stock and cart when remove item
		// Save file
		
		input.close();
		System.out.println("Ended!"); // Program ended
		
	}
		
   }

		
		
		
		
	