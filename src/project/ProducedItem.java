//Create ProducedItem so that we can call it again in the method the app afterthat, meaning customer can search it in the app
// Author's name: Thu Tran, date: May 28, 2022
package project;

public class ProducedItem extends ShelvedItem {
	
	public String date;
	
	 // default constructor
	public ProducedItem() {
		this.date = "";
	}
	  // custom constructor
	public ProducedItem(String name, double price, String date) {
		super(name,price);
		this.date = date;
	}
	 // mutator methods
	public void setDate (String date) {
		this.date = date;
	}
	//accesor method
	public String getDate() {
		return date;
	}
	
	//toString format
	@Override
	public String toString() {
		return "Product: "  + getName() + " Price:" + getPrice() + " Experation date: " + date;
	}
	public boolean equals(Object obj) {

		if (!(obj instanceof ProducedItem )) {
			return false;
		}
		else  {
			ProducedItem p = (ProducedItem) obj;
			if (this.date ==(p.getDate())) {
				return true;
			}
			else {
			     return false;
			}
    }

 }
}




