// Create Shelved Item class. This act as the main class for the two inhereted class after that which is AgeResItem and Produced Item 
// Name: Thu Tran, date: May 28 2022
package project;

public class ShelvedItem {
	
// Part 1: creating variables
	private String name;
    private double price;
    private String date;
    private int age;
    
    
    // default constructor
    public ShelvedItem(){ // need to be exact 
        // default values will be 0
        this.name = "";
        this.price = 0;
        this.age = 0;
        this.date = "";
    }
    
    // custom constructor
    public ShelvedItem(String name,double price){
        this.name = name;
        this.price = price;
       
    }
    
    // accessor methods
    public String getName(){
        return name;
    }
    
    public double getPrice(){
        return price;
    }
    
    // mutator methods
    public void setName(String name){
        this.name = name;
    }
    public void setPrice(double price){
        this.price = price;
    }
   
    
    @Override
    public String toString(){
        return("("+ "Product name:" + this.name+", "+ "the price is: " +this.price +")");
    }
    
  
    
    
    public boolean isValidPrice(){
        if(this.price >= 0.0){
            return true;
        }
        else{
            return false;
	}
  }
    public boolean equals(Object obj) {

		if (!(obj instanceof ShelvedItem)) {
			return false;
		}
		else  {
			 ShelvedItem s = (ShelvedItem) obj;
			if (this.price ==(s.getPrice())&& this.name == s.getName())
				return true;
			else
			     return false;
			}
    }


	private String csvData() {
		// TODO Auto-generated method stub
	 	return name + "," + price + ".";
	}

	public String toCSV() {
		// TODO Auto-generated method stub
		return "Product," + csvData();
	}

	
	}


